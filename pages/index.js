import React, { useState, useEffect } from "react"
import Head from "next/head"
import Script from "next/script"
import Image from "next/image"
import styles from "../styles/Home.module.css"

export default () => {
  useEffect(() => {
    OmiseCard.configure({
      publicKey: "pkey_test_5awtfzpxdh80ufthhhn",
      frameLabel: "Gether Go",
      submitLabel: "Pay",
      location: "no",
      currency: "th",
      image: "http://localhost:3000/go-icon.png",
      // submitFormTarget: "#paymentForm",
    })

    OmiseCard.attach()
  }, [])

  const PayWithCreditCard = (_event) => {
    _event.preventDefault()

    const form = window.document.querySelector("#paymentForm")

    OmiseCard.open({
      amount: 12000,
      currency: "THB",
      defaultPaymentMethod: "credit_card",
      onCreateTokenSuccess: async (nonce) => {
        let createToken = {
          description: "Charge for order ID: 0001",
          amount: "13000",
          currency: "thb",
          capture: true,
          card: nonce,
          omiseToken: null,
          omiseSource: null,
        }

        if (nonce.startsWith("tokn_")) {
          createToken.omiseToken = nonce
        } else {
          createToken.omiseSource = nonce
        }

        const res = await fetch(`http://localhost:3000/api/checkout`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(createToken),
        }).then((r) => r.json())

        console.log("res : ", res)
      },
      onFormClosed: () => {
        /* Handler on form closure. */
        console.log("close omise modal")
      },
    })
  }

  return (
    <div className={styles.container}>
      <Head>
        <title>Demo Omise</title>
        <meta name="description" content="Demo Omise" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>
          Payment <a href="https://www.omise.co/th/thailand">Omise</a>
        </h1>

        <div className={styles.description}>
          <form id="paymentForm">
            <input id="omiseToken" name="omiseToken" type="hidden" />
            <input id="omiseSource" name="omiseSource" type="hidden" />

            <input
              type="submit"
              value="Pay With Credit Card"
              onClick={(event) => {
                PayWithCreditCard(event)
              }}
            />
          </form>

          {"  "}
          {/* <button>Pay With Internet Banking</button> */}
        </div>
      </main>

      <footer className={styles.footer}>
        <a
          href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
          target="_blank"
          rel="noopener noreferrer"
        >
          Powered by{" "}
          <span className={styles.logo}>
            <Image src="/vercel.svg" alt="Vercel Logo" width={72} height={16} />
          </span>
        </a>
      </footer>
    </div>
  )
}
