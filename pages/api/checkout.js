// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
var omise = require("omise")({
  secretKey: "skey_test_5awtfzpxygz8dqiw0nc",
  omiseVersion: "2015-09-10",
})

export default function handler(req, res) {
  if (req.method === "POST") {
    console.log("req : ", req.body)
    omise.charges.create(
      {
        description: req.body.description,
        amount: req.body.amount, // 1,000 Baht
        currency: req.body.currency,
        capture: req.body.capture,
        card: req.body.card,
      },
      (err, resp) => {
        console.log("resp.paid: ", resp)
        if (resp.paid) {
          res.status(200).json({ resp })
          //Success
        } else {
          //Handle failure
          console.log("failure_code:", resp.failure_code)
          throw resp.failure_code
        }
      }
    )
  } else {
    res.status(200).json({ name: "Hello" })
  }
}
